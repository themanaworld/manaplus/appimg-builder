#!/bin/bash
set -e

# clone repo

mkdir manaplus

cd manaplus

git clone https://git.themanaworld.org/mana/plus.git manaplus

cd manaplus

# compile

dir=`pwd`

autoreconf -i

CPPFLAGS="-DTCP_THIN_LINEAR_TIMEOUTS -DTCP_THIN_DUPACK" ./configure --prefix=$dir/run \
--enable-commandlinepassword \
--datadir=$dir/run/share/games \
--bindir=$dir/run/bin \
--mandir=$dir/run/share/man \
--enable-portable=yes

cd po
make update-gmo -j4
cd ..
make -j4
mkdir run
make install

# package
mkdir AppDir

cp -r $dir/run/share/games/manaplus/data AppDir/data
cp ../../AppRun AppDir/AppRun

../../squashfs-root/AppRun --appdir AppDir \
-d ../../manaplus.desktop \
-i ./data/icons/manaplus.svg \
-e ./run/bin/manaplus \
--output appimage

mv *.AppImage ../../

